#ifndef REGISTER_HPP
#define REGISTER_HPP
#include <string>
#include <iostream>
#include <map>

class Register{
    std::string aRegisterName;
    std::map<std::string,unsigned int> aFlags;
    public:
    Register(const char *name){
        aRegisterName=name;
    }
    void AddRegisterFlag(std::string name,const unsigned int value){
        aFlags.insert(std::pair<std::string,unsigned int>(name,value));
    }
    void PrintRegisterFlags(unsigned int value){
        for(auto it=aFlags.begin();it!=aFlags.end();++it){
            if(it->second&value){
                std::cout<<it->first<<std::endl;
            }
        }
    }
    void printRegisterName(){
        std::cout<<aRegisterName<<std::endl;
    }
    bool operator==(std::string name){
        if(aRegisterName==name){return true;}else{return false;}
    }
};
#endif /* REGISTER_HPP */
