# GetFlagFromReg
Prints set bits(flags) names of some STM32 family procesor register from passed value. Application helps debugging causes of program HardFaults, saving time spent looking for description of specified Register in datasheet and then counting bits or deciphering Hex values.
## Getting Started
Application needs to be compiled from source code and used in terminal
### Prerequisites
Application requaires boost libarary.
### Installing
To install application simply copy executable to folder which is in system PATH.
## Contributing
If You would like to add some new registers submit pull request
## Authors
kazmierz18
## License
This project is licensed under the "THE BEER-WARE LICENSE"