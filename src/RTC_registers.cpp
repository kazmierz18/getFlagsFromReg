/********************  Bits definition for RTC_CR reg  *******************/
#define RTC_CR_ITSE                          0x01000000U 
#define RTC_CR_COE                           0x00800000U
#define RTC_CR_OSEL                          0x00600000U
#define RTC_CR_OSEL_0                        0x00200000U
#define RTC_CR_OSEL_1                        0x00400000U
#define RTC_CR_POL                           0x00100000U
#define RTC_CR_COSEL                         0x00080000U
#define RTC_CR_BCK                           0x00040000U
#define RTC_CR_SUB1H                         0x00020000U
#define RTC_CR_ADD1H                         0x00010000U
#define RTC_CR_TSIE                          0x00008000U
#define RTC_CR_WUTIE                         0x00004000U
#define RTC_CR_ALRBIE                        0x00002000U
#define RTC_CR_ALRAIE                        0x00001000U
#define RTC_CR_TSE                           0x00000800U
#define RTC_CR_WUTE                          0x00000400U
#define RTC_CR_ALRBE                         0x00000200U
#define RTC_CR_ALRAE                         0x00000100U
#define RTC_CR_FMT                           0x00000040U
#define RTC_CR_BYPSHAD                       0x00000020U
#define RTC_CR_REFCKON                       0x00000010U
#define RTC_CR_TSEDGE                        0x00000008U
#define RTC_CR_WUCKSEL                       0x00000007U
#define RTC_CR_WUCKSEL_0                     0x00000001U
#define RTC_CR_WUCKSEL_1                     0x00000002U
#define RTC_CR_WUCKSEL_2                     0x00000004U

/********************  Bits definition for RTC_ISR reg  ******************/
#define RTC_ISR_ITSF                         0x00020000U
#define RTC_ISR_RECALPF                      0x00010000U
#define RTC_ISR_TAMP3F                       0x00008000U
#define RTC_ISR_TAMP2F                       0x00004000U
#define RTC_ISR_TAMP1F                       0x00002000U
#define RTC_ISR_TSOVF                        0x00001000U
#define RTC_ISR_TSF                          0x00000800U
#define RTC_ISR_WUTF                         0x00000400U
#define RTC_ISR_ALRBF                        0x00000200U
#define RTC_ISR_ALRAF                        0x00000100U
#define RTC_ISR_INIT                         0x00000080U
#define RTC_ISR_INITF                        0x00000040U
#define RTC_ISR_RSF                          0x00000020U
#define RTC_ISR_INITS                        0x00000010U
#define RTC_ISR_SHPF                         0x00000008U
#define RTC_ISR_WUTWF                        0x00000004U
#define RTC_ISR_ALRBWF                       0x00000002U
#define RTC_ISR_ALRAWF                       0x00000001U

#include <vector>
#include <Register.hpp>
extern std::vector<Register> registers;


static void addCRreg(){
    Register reg("RTC_CR");
    reg.AddRegisterFlag("RTC_CR_ITSE",RTC_CR_ITSE);
    reg.AddRegisterFlag("RTC_CR_COE",RTC_CR_COE);
    reg.AddRegisterFlag("RTC_CR_OSEL_0",RTC_CR_OSEL_0);
    reg.AddRegisterFlag("RTC_CR_OSEL_1",RTC_CR_OSEL_1);
    reg.AddRegisterFlag("RTC_CR_POL",RTC_CR_POL);
    reg.AddRegisterFlag("RTC_CR_COSEL",RTC_CR_COSEL);
    reg.AddRegisterFlag("RTC_CR_BCK",RTC_CR_BCK);
    reg.AddRegisterFlag("RTC_CR_SUB1H",RTC_CR_SUB1H);
    reg.AddRegisterFlag("RTC_CR_ADD1H",RTC_CR_ADD1H);
    reg.AddRegisterFlag("RTC_CR_TSIE",RTC_CR_TSIE);
    reg.AddRegisterFlag("RTC_CR_WUTIE",RTC_CR_WUTIE);
    reg.AddRegisterFlag("RTC_CR_ALRBIE",RTC_CR_ALRBIE);
    reg.AddRegisterFlag("RTC_CR_ALRAIE",RTC_CR_ALRAIE);
    reg.AddRegisterFlag("RTC_CR_TSE",RTC_CR_TSE);
    reg.AddRegisterFlag("RTC_CR_WUTE",RTC_CR_WUTE);
    reg.AddRegisterFlag("RTC_CR_ALRBE",RTC_CR_ALRBE);
    reg.AddRegisterFlag("RTC_CR_ALRAE",RTC_CR_ALRAE);
    reg.AddRegisterFlag("RTC_CR_FMT",RTC_CR_FMT);
    reg.AddRegisterFlag("RTC_CR_BYPSHAD",RTC_CR_BYPSHAD);
    reg.AddRegisterFlag("RTC_CR_REFCKON",RTC_CR_REFCKON);
    reg.AddRegisterFlag("RTC_CR_TSEDGE",RTC_CR_TSEDGE);
    reg.AddRegisterFlag("RTC_CR_WUCKSEL_0",RTC_CR_WUCKSEL_0);
    reg.AddRegisterFlag("RTC_CR_WUCKSEL_1",RTC_CR_WUCKSEL_1);
    reg.AddRegisterFlag("RTC_CR_WUCKSEL_2",RTC_CR_WUCKSEL_2);
    registers.push_back(reg);
}
static void addISRReg(){
    Register reg("RTC_ISR");
    reg.AddRegisterFlag("RTC_ISR_ITSF",RTC_ISR_ITSF);
    reg.AddRegisterFlag("RTC_ISR_RECALPF",RTC_ISR_RECALPF);
    reg.AddRegisterFlag("RTC_ISR_TAMP3F",RTC_ISR_TAMP3F);
    reg.AddRegisterFlag("RTC_ISR_TAMP2F",RTC_ISR_TAMP2F);
    reg.AddRegisterFlag("RTC_ISR_TAMP1F",RTC_ISR_TAMP1F);
    reg.AddRegisterFlag("RTC_ISR_TSOVF",RTC_ISR_TSOVF);
    reg.AddRegisterFlag("RTC_ISR_TSF",RTC_ISR_TSF);
    reg.AddRegisterFlag("RTC_ISR_WUTF",RTC_ISR_WUTF);
    reg.AddRegisterFlag("RTC_ISR_ALRBF",RTC_ISR_ALRBF);
    reg.AddRegisterFlag("RTC_ISR_ALRAF",RTC_ISR_ALRAF);
    reg.AddRegisterFlag("RTC_ISR_INIT",RTC_ISR_INIT);
    reg.AddRegisterFlag("RTC_ISR_INITF",RTC_ISR_INITF);
    reg.AddRegisterFlag("RTC_ISR_RSF",RTC_ISR_RSF);
    reg.AddRegisterFlag("RTC_ISR_INITS",RTC_ISR_INITS);
    reg.AddRegisterFlag("RTC_ISR_SHPF",RTC_ISR_SHPF);
    reg.AddRegisterFlag("RTC_ISR_WUTWF",RTC_ISR_WUTWF);
    reg.AddRegisterFlag("RTC_ISR_ALRBWF",RTC_ISR_ALRBWF);
    reg.AddRegisterFlag("RTC_ISR_ALRAWF",RTC_ISR_ALRAWF);
    registers.push_back(reg);
}

void addRTCregs(){
    addCRreg();
    addISRReg();
}
