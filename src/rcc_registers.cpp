/********************  Bit definition for RCC_BDCR register  ******************/
#define  RCC_BDCR_LSEON                      0x00000001U
#define  RCC_BDCR_LSERDY                     0x00000002U
#define  RCC_BDCR_LSEBYP                     0x00000004U
#define  RCC_BDCR_LSEDRV                     0x00000018U
#define  RCC_BDCR_LSEDRV_0                   0x00000008U
#define  RCC_BDCR_LSEDRV_1                   0x00000010U
#define  RCC_BDCR_RTCSEL                     0x00000300U
#define  RCC_BDCR_RTCSEL_0                   0x00000100U
#define  RCC_BDCR_RTCSEL_1                   0x00000200U
#define  RCC_BDCR_RTCEN                      0x00008000U
#define  RCC_BDCR_BDRST                      0x00010000U

/********************  Bit definition for RCC_CSR register  *******************/
#define  RCC_CSR_LSION                       0x00000001U
#define  RCC_CSR_LSIRDY                      0x00000002U
#define  RCC_CSR_RMVF                        0x01000000U
#define  RCC_CSR_BORRSTF                     0x02000000U
#define  RCC_CSR_PINRSTF                     0x04000000U
#define  RCC_CSR_PORRSTF                     0x08000000U
#define  RCC_CSR_SFTRSTF                     0x10000000U
#define  RCC_CSR_IWDGRSTF                    0x20000000U
#define  RCC_CSR_WWDGRSTF                    0x40000000U
#define  RCC_CSR_LPWRRSTF                    0x80000000U


#include <vector>
#include "Register.hpp"

extern std::vector<Register> registers;

static void addBDCRreg(){
    Register reg("RCC_BDCR");
    reg.AddRegisterFlag("RCC_BDCR_LSEON",RCC_BDCR_LSEON);
    reg.AddRegisterFlag("RCC_BDCR_LSERDY",RCC_BDCR_LSERDY);
    reg.AddRegisterFlag("RCC_BDCR_LSEBYP",RCC_BDCR_LSEBYP);
    reg.AddRegisterFlag("RCC_BDCR_LSEDRV_0",RCC_BDCR_LSEDRV_0);
    reg.AddRegisterFlag("RCC_BDCR_LSEDRV_1",RCC_BDCR_LSEDRV_1);
    reg.AddRegisterFlag("RCC_BDCR_RTCSEL_0",RCC_BDCR_RTCSEL_0);
    reg.AddRegisterFlag("RCC_BDCR_RTCSEL_1",RCC_BDCR_RTCSEL_1);
    reg.AddRegisterFlag("RCC_BDCR_RTCEN",RCC_BDCR_RTCEN);
    reg.AddRegisterFlag("RCC_BDCR_BDRST",RCC_BDCR_BDRST);

    registers.push_back(reg);
}

static void addCSRreg(){
    Register reg("RCC_CSR");
    reg.AddRegisterFlag("RCC_CSR_LSION",RCC_CSR_LSION);
    reg.AddRegisterFlag("RCC_CSR_LSIRDY",RCC_CSR_LSIRDY);
    reg.AddRegisterFlag("RCC_CSR_RMVF",RCC_CSR_RMVF);
    reg.AddRegisterFlag("RCC_CSR_BORRSTF",RCC_CSR_BORRSTF);
    reg.AddRegisterFlag("RCC_CSR_PINRSTF",RCC_CSR_PINRSTF);
    reg.AddRegisterFlag("RCC_CSR_PORRSTF",RCC_CSR_PORRSTF);
    reg.AddRegisterFlag("RCC_CSR_SFTRSTF",RCC_CSR_SFTRSTF);
    reg.AddRegisterFlag("RCC_CSR_IWDGRSTF",RCC_CSR_IWDGRSTF);
    reg.AddRegisterFlag("RCC_CSR_WWDGRSTF",RCC_CSR_WWDGRSTF);
    reg.AddRegisterFlag("RCC_CSR_LPWRRSTF",RCC_CSR_LPWRRSTF);

    registers.push_back(reg);
}

void addRCCreg(){
    addBDCRreg();
    addCSRreg();
}