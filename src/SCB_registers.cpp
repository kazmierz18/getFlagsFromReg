#include <vector>
#include "Register.hpp"

extern std::vector<Register> registers;

/* SCB Configurable Fault Status Register Definitions */
#define SCB_CFSR_USGFAULTSR_Pos            16U                                            /*!< SCB CFSR: Usage Fault Status Register Position */
#define SCB_CFSR_USGFAULTSR_Msk            (0xFFFFUL << SCB_CFSR_USGFAULTSR_Pos)          /*!< SCB CFSR: Usage Fault Status Register Mask */

#define SCB_CFSR_BUSFAULTSR_Pos             8U                                            /*!< SCB CFSR: Bus Fault Status Register Position */
#define SCB_CFSR_BUSFAULTSR_Msk            (0xFFUL << SCB_CFSR_BUSFAULTSR_Pos)            /*!< SCB CFSR: Bus Fault Status Register Mask */

#define SCB_CFSR_MEMFAULTSR_Pos             0U                                            /*!< SCB CFSR: Memory Manage Fault Status Register Position */
#define SCB_CFSR_MEMFAULTSR_Msk            (0xFFUL /*<< SCB_CFSR_MEMFAULTSR_Pos*/)        /*!< SCB CFSR: Memory Manage Fault Status Register Mask */

static void addCFSRreg(){
    Register reg("SCB_CFSR");
    reg.AddRegisterFlag("MMFSR-MMARVALID",1<<(7+SCB_CFSR_MEMFAULTSR_Pos));
    reg.AddRegisterFlag("MMFSR-MLSPERR",1<<(5+SCB_CFSR_MEMFAULTSR_Pos));
    reg.AddRegisterFlag("MMFSR-MSTKERR",1<<(4+SCB_CFSR_MEMFAULTSR_Pos));
    reg.AddRegisterFlag("MMFSR-MUNSTKERR",1<<(3+SCB_CFSR_MEMFAULTSR_Pos));
    reg.AddRegisterFlag("MMFSR-DACCVIOL",1<<(1+SCB_CFSR_MEMFAULTSR_Pos));
    reg.AddRegisterFlag("MMFSR-IACCVIOL",1<<(0+SCB_CFSR_MEMFAULTSR_Pos));

    reg.AddRegisterFlag("BFSR-BFARVALID",1<<(7+SCB_CFSR_BUSFAULTSR_Pos));
    reg.AddRegisterFlag("BFSR-LSPERR",1<<(5+SCB_CFSR_BUSFAULTSR_Pos));
    reg.AddRegisterFlag("BFSR-STKERR",1<<(4+SCB_CFSR_BUSFAULTSR_Pos));
    reg.AddRegisterFlag("BFSR-UNSTKERR",1<<(3+SCB_CFSR_BUSFAULTSR_Pos));
    reg.AddRegisterFlag("BFSR-IMPRECISERR",1<<(2+SCB_CFSR_BUSFAULTSR_Pos));
    reg.AddRegisterFlag("BFSR-PRECISERR",1<<(1+SCB_CFSR_BUSFAULTSR_Pos));
    reg.AddRegisterFlag("BFSR-IBUSERR",1<<(0+SCB_CFSR_BUSFAULTSR_Pos));

    reg.AddRegisterFlag("UFSR-DIVBYZERO",1<<(9+SCB_CFSR_USGFAULTSR_Pos));
    reg.AddRegisterFlag("UFSR-UNALIGNED",1<<(8+SCB_CFSR_USGFAULTSR_Pos));
    reg.AddRegisterFlag("UFSR-NOCP",1<<(3+SCB_CFSR_USGFAULTSR_Pos));
    reg.AddRegisterFlag("UFSR-INVPC",1<<(2+SCB_CFSR_USGFAULTSR_Pos));
    reg.AddRegisterFlag("UFSR-INVSTATE",1<<(1+SCB_CFSR_USGFAULTSR_Pos));
    reg.AddRegisterFlag("UFSR-UNDEFINSTR",1<<(0+SCB_CFSR_USGFAULTSR_Pos));

    registers.push_back(reg);
}

/* SCB Interrupt Control State Register Definitions */
#define SCB_ICSR_NMIPENDSET_Pos            31U                                            /*!< SCB ICSR: NMIPENDSET Position */
#define SCB_ICSR_NMIPENDSET_Msk            (1UL << SCB_ICSR_NMIPENDSET_Pos)               /*!< SCB ICSR: NMIPENDSET Mask */

#define SCB_ICSR_PENDSVSET_Pos             28U                                            /*!< SCB ICSR: PENDSVSET Position */
#define SCB_ICSR_PENDSVSET_Msk             (1UL << SCB_ICSR_PENDSVSET_Pos)                /*!< SCB ICSR: PENDSVSET Mask */

#define SCB_ICSR_PENDSVCLR_Pos             27U                                            /*!< SCB ICSR: PENDSVCLR Position */
#define SCB_ICSR_PENDSVCLR_Msk             (1UL << SCB_ICSR_PENDSVCLR_Pos)                /*!< SCB ICSR: PENDSVCLR Mask */

#define SCB_ICSR_PENDSTSET_Pos             26U                                            /*!< SCB ICSR: PENDSTSET Position */
#define SCB_ICSR_PENDSTSET_Msk             (1UL << SCB_ICSR_PENDSTSET_Pos)                /*!< SCB ICSR: PENDSTSET Mask */

#define SCB_ICSR_PENDSTCLR_Pos             25U                                            /*!< SCB ICSR: PENDSTCLR Position */
#define SCB_ICSR_PENDSTCLR_Msk             (1UL << SCB_ICSR_PENDSTCLR_Pos)                /*!< SCB ICSR: PENDSTCLR Mask */

#define SCB_ICSR_ISRPREEMPT_Pos            23U                                            /*!< SCB ICSR: ISRPREEMPT Position */
#define SCB_ICSR_ISRPREEMPT_Msk            (1UL << SCB_ICSR_ISRPREEMPT_Pos)               /*!< SCB ICSR: ISRPREEMPT Mask */

#define SCB_ICSR_ISRPENDING_Pos            22U                                            /*!< SCB ICSR: ISRPENDING Position */
#define SCB_ICSR_ISRPENDING_Msk            (1UL << SCB_ICSR_ISRPENDING_Pos)               /*!< SCB ICSR: ISRPENDING Mask */

#define SCB_ICSR_VECTPENDING_Pos           12U                                            /*!< SCB ICSR: VECTPENDING Position */
#define SCB_ICSR_VECTPENDING_Msk           (0x1FFUL << SCB_ICSR_VECTPENDING_Pos)          /*!< SCB ICSR: VECTPENDING Mask */

#define SCB_ICSR_RETTOBASE_Pos             11U                                            /*!< SCB ICSR: RETTOBASE Position */
#define SCB_ICSR_RETTOBASE_Msk             (1UL << SCB_ICSR_RETTOBASE_Pos)                /*!< SCB ICSR: RETTOBASE Mask */

#define SCB_ICSR_VECTACTIVE_Pos             0U                                            /*!< SCB ICSR: VECTACTIVE Position */
#define SCB_ICSR_VECTACTIVE_Msk            (0x1FFUL /*<< SCB_ICSR_VECTACTIVE_Pos*/)       /*!< SCB ICSR: VECTACTIVE Mask */



static void addICSRReg(){
    Register reg("SCB_ICSR");
    reg.AddRegisterFlag("SCB_ICSR_NMIPENDSET_Msk",SCB_ICSR_NMIPENDSET_Msk);
    reg.AddRegisterFlag("SCB_ICSR_PENDSVSET_Msk",SCB_ICSR_PENDSVSET_Msk);
    reg.AddRegisterFlag("SCB_ICSR_PENDSVCLR_Msk",SCB_ICSR_PENDSVCLR_Msk);
    reg.AddRegisterFlag("SCB_ICSR_PENDSTSET_Msk",SCB_ICSR_PENDSTSET_Msk);
    reg.AddRegisterFlag("SCB_ICSR_PENDSTCLR_Msk",SCB_ICSR_PENDSTCLR_Msk);
    reg.AddRegisterFlag("SCB_ICSR_ISRPREEMPT_Msk",SCB_ICSR_ISRPREEMPT_Msk);
    reg.AddRegisterFlag("SCB_ICSR_ISRPENDING_Msk",SCB_ICSR_ISRPENDING_Msk);
    reg.AddRegisterFlag("SCB_ICSR_VECTPENDING_Msk",SCB_ICSR_VECTPENDING_Msk);
    reg.AddRegisterFlag("SCB_ICSR_RETTOBASE_Msk",SCB_ICSR_RETTOBASE_Msk);
    reg.AddRegisterFlag("SCB_ICSR_VECTACTIVE_Msk",SCB_ICSR_VECTACTIVE_Msk);

    registers.push_back(reg);
    
}

void addSCBregs(){
    addCFSRreg();
    addICSRReg();
}