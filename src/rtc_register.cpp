
#include<iostream>
#include <string>
#include "registers.hpp"


/********************  Bits definition for RTC_CR register  *******************/
#define RTC_CR_ITSE                          0x01000000U 
#define RTC_CR_COE                           0x00800000U
#define RTC_CR_OSEL                          0x00600000U
#define RTC_CR_OSEL_0                        0x00200000U
#define RTC_CR_OSEL_1                        0x00400000U
#define RTC_CR_POL                           0x00100000U
#define RTC_CR_COSEL                         0x00080000U
#define RTC_CR_BCK                           0x00040000U
#define RTC_CR_SUB1H                         0x00020000U
#define RTC_CR_ADD1H                         0x00010000U
#define RTC_CR_TSIE                          0x00008000U
#define RTC_CR_WUTIE                         0x00004000U
#define RTC_CR_ALRBIE                        0x00002000U
#define RTC_CR_ALRAIE                        0x00001000U
#define RTC_CR_TSE                           0x00000800U
#define RTC_CR_WUTE                          0x00000400U
#define RTC_CR_ALRBE                         0x00000200U
#define RTC_CR_ALRAE                         0x00000100U
#define RTC_CR_FMT                           0x00000040U
#define RTC_CR_BYPSHAD                       0x00000020U
#define RTC_CR_REFCKON                       0x00000010U
#define RTC_CR_TSEDGE                        0x00000008U
#define RTC_CR_WUCKSEL                       0x00000007U
#define RTC_CR_WUCKSEL_0                     0x00000001U
#define RTC_CR_WUCKSEL_1                     0x00000002U
#define RTC_CR_WUCKSEL_2                     0x00000004U


/********************  Bits definition for RTC_ISR register  ******************/
#define RTC_ISR_ITSF                         0x00020000U
#define RTC_ISR_RECALPF                      0x00010000U
#define RTC_ISR_TAMP3F                       0x00008000U
#define RTC_ISR_TAMP2F                       0x00004000U
#define RTC_ISR_TAMP1F                       0x00002000U
#define RTC_ISR_TSOVF                        0x00001000U
#define RTC_ISR_TSF                          0x00000800U
#define RTC_ISR_WUTF                         0x00000400U
#define RTC_ISR_ALRBF                        0x00000200U
#define RTC_ISR_ALRAF                        0x00000100U
#define RTC_ISR_INIT                         0x00000080U
#define RTC_ISR_INITF                        0x00000040U
#define RTC_ISR_RSF                          0x00000020U
#define RTC_ISR_INITS                        0x00000010U
#define RTC_ISR_SHPF                         0x00000008U
#define RTC_ISR_WUTWF                        0x00000004U
#define RTC_ISR_ALRBWF                       0x00000002U
#define RTC_ISR_ALRAWF                       0x00000001U


int RTC_CR_register(unsigned int x){
    if(RTC_CR_ITSE&x){
        std::cout<<"RTC_CR_ITSE"<<std::endl;
    }
    if(RTC_CR_COE&x){
        std::cout<<"RTC_CR_COE"<<std::endl;
    }
    if(RTC_CR_OSEL_0&x){
        std::cout<<"RTC_CR_OSEL_0"<<std::endl;
    }
    if(RTC_CR_OSEL_1&x){
        std::cout<<"RTC_CR_OSEL_1"<<std::endl;
    }
    if(RTC_CR_POL&x){
        std::cout<<"RTC_CR_POL"<<std::endl;
    }
    if(RTC_CR_COSEL&x){
        std::cout<<"RTC_CR_COSEL"<<std::endl;
    }
    if(RTC_CR_BCK&x){
        std::cout<<"RTC_CR_BCK"<<std::endl;
    }
    if(RTC_CR_SUB1H&x){
        std::cout<<"RTC_CR_SUB1H"<<std::endl;
    }
    if(RTC_CR_ADD1H&x){
        std::cout<<"RTC_CR_ADD1H"<<std::endl;
    }
    if(RTC_CR_BCK&x){
        std::cout<<"RTC_CR_BCK"<<std::endl;
    }
    if(RTC_CR_SUB1H&x){
        std::cout<<"RTC_CR_SUB1H"<<std::endl;
    }
    if(RTC_CR_ADD1H&x){
        std::cout<<"RTC_CR_ADD1H"<<std::endl;
    }
    if(RTC_CR_TSIE&x){
        std::cout<<"RTC_CR_TSIE"<<std::endl;
    }
    if(RTC_CR_WUTIE&x){
        std::cout<<"RTC_CR_WUTIE"<<std::endl;
    }
    if(RTC_CR_ALRBIE&x){
        std::cout<<"RTC_CR_ALRBIE"<<std::endl;
    }
    if(RTC_CR_ALRAIE&x){
        std::cout<<"RTC_CR_ALRAIE"<<std::endl;
    }
    if(RTC_CR_TSE&x){
        std::cout<<"RTC_CR_TSE"<<std::endl;
    }
    if(RTC_CR_WUTE&x){
        std::cout<<"RTC_CR_WUTE"<<std::endl;
    }
    if(RTC_CR_ALRBE&x){
        std::cout<<"RTC_CR_ALRBE"<<std::endl;
    }
    if(RTC_CR_ALRAE&x){
        std::cout<<"RTC_CR_ALRAE"<<std::endl;
    }
    if(RTC_CR_FMT&x){
        std::cout<<"RTC_CR_FMT"<<std::endl;
    }
    if(RTC_CR_BYPSHAD&x){
        std::cout<<"RTC_CR_BYPSHAD"<<std::endl;
    }
    if(RTC_CR_REFCKON&x){
        std::cout<<"RTC_CR_REFCKON"<<std::endl;
    }
    if(RTC_CR_TSEDGE&x){
        std::cout<<"RTC_CR_TSEDGE"<<std::endl;
    }
    if(RTC_CR_WUCKSEL_0&x){
        std::cout<<"RTC_CR_WUCKSEL_0"<<std::endl;
    }
    if(RTC_CR_WUCKSEL_1&x){
        std::cout<<"RTC_CR_WUCKSEL_1"<<std::endl;
    }
    if(RTC_CR_WUCKSEL_2&x){
        std::cout<<"RTC_CR_WUCKSEL_2"<<std::endl;
    }
    return 0;
}

int RTC_ISR_register(unsigned int x){
    if(RTC_ISR_ITSF&x){
        std::cout<<"RTC_ISR_ITSF"<<std::endl;
    }
    if(RTC_ISR_RECALPF&x){
        std::cout<<"RTC_ISR_RECALPF"<<std::endl;
    }  
    if(RTC_ISR_TAMP3F&x){
        std::cout<<"RTC_ISR_TAMP3F"<<std::endl;
    }  
    if(RTC_ISR_TAMP2F&x){
        std::cout<<"RTC_ISR_TAMP2F"<<std::endl;
    }  
    if(RTC_ISR_TAMP1F&x){
        std::cout<<"RTC_ISR_TAMP1F"<<std::endl;
    }  
    if(RTC_ISR_TSOVF&x){
        std::cout<<"RTC_ISR_TSOVF"<<std::endl;
    }  
    if(RTC_ISR_WUTF&x){
        std::cout<<"RTC_ISR_WUTF"<<std::endl;
    }  
    if(RTC_ISR_ALRBF&x){
        std::cout<<"RTC_ISR_ALRBF"<<std::endl;
    }
    if(RTC_ISR_ALRAF&x){
        std::cout<<"RTC_ISR_ALRAF"<<std::endl;
    }  
    if(RTC_ISR_INIT&x){
        std::cout<<"RTC_ISR_INIT"<<std::endl;
    }  
    if(RTC_ISR_INITF&x){
        std::cout<<"RTC_ISR_INITF"<<std::endl;
    }  
    if(RTC_ISR_RSF&x){
        std::cout<<"RTC_ISR_RSF"<<std::endl;
    }  
    if(RTC_ISR_INITS&x){
        std::cout<<"RTC_ISR_INITS"<<std::endl;
    }  
    if(RTC_ISR_SHPF&x){
        std::cout<<"RTC_ISR_SHPF"<<std::endl;
    }  
    if(RTC_ISR_WUTWF&x){
        std::cout<<"RTC_ISR_WUTWF"<<std::endl;
    }  
    if(RTC_ISR_ALRBWF&x){
        std::cout<<"RTC_ISR_ALRBWF"<<std::endl;
    }  
    if(RTC_ISR_ALRAWF&x){
        std::cout<<"RTC_ISR_ALRAWF"<<std::endl;
    } 
    return 0;
}

