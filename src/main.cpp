#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <boost/program_options.hpp>
#include "../inc/registers.hpp"
#include "../inc/Register.hpp"

std::vector<Register> registers;

bool printRegister(const std::string &rType, const std::string &rVal)
{
    unsigned int x;
    std::stringstream ss;
    ss << std::hex << rVal;
    ss >> x;
    for (auto it = registers.begin(); it != registers.end(); ++it)
    {
        if (*it == rType)
        {
            it->PrintRegisterFlags(x);
            return true;
        }
    }
    std::cerr<<"Register not found try -l option to list all or use -h to see help"<<std::endl;
    return false;
}

int main(int argc, char *argv[])
{
    addRTCregs();
    addRCCreg();
    addSCBregs();
    std::string  registerType;
    std::string  registerVal;
    try
    {
        namespace po = boost::program_options;
        po::options_description desc("Options");
        desc.add_options()("help,h", "Print help messages")
        ("version,v", "Display the version number")
        ("list,l", "List all supported registers")
        ("register-name,r", po::value<std::string>(&registerType)->required(), "name of the register")
        ("register-val,w", po::value<std::string>(&registerVal)->required(), "value(hex) of the register");

        po::variables_map vm;
        try
        {
            po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);

            po::notify(vm);
            if (vm.count("help"))
            {
                std::cout << "Basic Command Line Parameter App" << std::endl
                          << desc << std::endl;
                return EXIT_SUCCESS;
            }
            if (vm.count("version"))
            {
                std::cout << "0.0.0" << std::endl;
                return EXIT_SUCCESS;
            }
            if (vm.count("list"))
            {
                std::cout << "Lista rejestrow:" << std::endl;
                for (auto it = registers.begin(); it != registers.end(); ++it)
                {
                    it->printRegisterName();
                }
                return EXIT_SUCCESS;
            }
            // if (vm.count("register-name") && vm.count("register-value"))
            // {
                return printRegister(registerType,registerVal)?EXIT_SUCCESS:EXIT_FAILURE;
            // }else{
            //     std::cerr << "Incorrect number of params, usage:\n getFlagsFromReg  reg_name reg_val"<< std::endl;
            //     return EXIT_FAILURE;
            // }
            
        }
        catch (po::error &e)
        {
            std::cerr << "ERROR: " << e.what() << std::endl
                      << std::endl;
            std::cerr << desc << std::endl;
            return EXIT_FAILURE;
        }

        // application code here //
    }
    catch (std::exception &e)
    {
        std::cerr << "Unhandled Exception reached the top of main: "
                  << e.what() << ", application will now exit" << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}